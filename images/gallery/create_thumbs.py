from PIL import Image
from glob import glob
import os

src_path = "fulls"
dest_path = "thumbs"

imgs = glob(src_path + os.sep + '*.jpg')

for f in imgs:
  print("File: ", f)

  dest_f = dest_path + os.sep + os.path.basename(f)

  img = Image.open(f)
  width, height = img.size
  new_size = (width//2, height//2)
  resized_image = img.resize(new_size)
  resized_image.save(dest_f,
                     optimize=True, quality=60)

  original_size = os.path.getsize(f)
  compressed_size = os.path.getsize(dest_f)

  print("Original Size:  ", original_size)
  print("Compressed Size: ", compressed_size)
  print()
